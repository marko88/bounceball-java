
import java.util.Date;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 * Platno igre, definise prostor igre i kome se nalaze plocice igraca i loptica.
 * Platno upravlja pravilima igre, odbijanjem od zidova ili plocica igraca i
 * upisivanje poena igracima.
 * Sadrzi i StatusPane, sto je platno za prikaz informacija o igracima.
 * Radi po principu Timeline animacije koja svake milisekunde poziva metodu moveBall()
 * koja pomera loptu po platnu i proverava kontakt lopte sa zidovima ili igracima i
 * upisuje poene.
 *
 * @author Marko
 */
class BallPane extends Pane {
    private Player playerDown;
    private Player playerUp;
    
    private final Label welcomeLabel;
    private final Label statusPlayer1Label;
    private final Label statusPlayer2Label;
    private final Label statusPlayer1CounterLabel;
    private final Label statusPlayer2CounterLabel;
    
    private Ball ball = null;
    private final GridPane statusPane;
    private double dx = 1;
    private double dy = 1;
    private double spin = 1;
    private final Timeline animation;
    private long startTimeForCycle;
    private final int intervalBeforeSpeedUp = 2000;
    
    private double lastBouceBallX;
    private double lastBouceBallY;
    private double lastBouceSpin;
    private double lastBouceDX;
    private double lastBouceDY;
    
    public BallPane(Player playerUp ,Player playerDown) {
        // Podesavanje statusPane panela
        statusPane = new GridPane();
        statusPane.setPadding(new Insets(20));
        statusPlayer1Label = new Label(playerDown.getName() + " : ");
        statusPlayer2Label = new Label(playerUp.getName() + " : ");
        String playerUpScore = (playerUp.isComputer()) ? String.valueOf(GameDB.getComputerScore()) : "0";
        String playerDownScore = (playerUp.isComputer()) ? String.valueOf(GameDB.getPlayerScore()) : "0";
        statusPlayer1CounterLabel = new Label(playerDownScore);
        statusPlayer2CounterLabel = new Label(playerUpScore);
        statusPane.add(statusPlayer1Label, 0, 0);
        statusPane.add(statusPlayer1CounterLabel, 1, 0);
        statusPane.add(statusPlayer2Label, 0, 1);
        statusPane.add(statusPlayer2CounterLabel, 1, 1);
        statusPane.setStyle("-fx-background-color: #ddd");
        statusPane.setPrefSize(200, 500);
        statusPane.setLayoutX(500);
        statusPlayer1Label.setFont(new Font(16));
        statusPlayer2Label.setFont(new Font(16));
        statusPlayer1CounterLabel.setFont(new Font(16));
        statusPlayer2CounterLabel.setFont(new Font(16));
        
        // Podesavanje prostora za igru, ubacivanje loptice, igraca i podesavanje
        // velicine prozora
        this.playerDown = playerDown;
        this.playerUp = playerUp;
        ball = new Ball();
        playerUp.setPane(this);
        playerDown.setPane(this);
        setWidth(GameRegistry.getInt("GAME_PANE_WIDTH"));
        setHeight(GameRegistry.getInt("GAME_PANE_HEIGHT"));
        setStyle("-fx-background-color: #000;");
        getChildren().add(statusPane);
        getChildren().add(ball.getCircle()); // Postavljanje lopte u okno
        
        // Kreiranje animacije sa kretanjem lopte
        animation = new Timeline(new KeyFrame(Duration.millis(1), e -> moveBall()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.stop();
        
        // Podesavanje Welcome labele
        welcomeLabel = new Label("<Press Left Click to START>");
        welcomeLabel.setTextFill(Color.WHITE);
        welcomeLabel.setFont(new Font(15));
        welcomeLabel.setLayoutX(170);
        welcomeLabel.setLayoutY(240);
        
        getChildren().add(playerDown.getRectangle());
        getChildren().add(playerUp.getRectangle());
        getChildren().add(welcomeLabel);
        setAllStrtingPositions();
    }

    /**
     * Metoda za pomeranje loptice po platnu.
     * Regulise odbijanje loptice i upisivanje poena igracima
     * 
     * @return void
     */
    protected void moveBall() {
        
        // Ubrzava igru u odnosu na interval getIntervalBeforeSpeedUp()
        if(new Date().getTime() > getStartTimeForCycle() + getIntervalBeforeSpeedUp()) {
            increaseSpeed();
            setStartTimeForCycle(new Date().getTime());
        }
        
        // Povlaci potez racunara
        getPlayerDown().computerMoveUpdate();
        getPlayerUp().computerMoveUpdate();
        
        // Odbijanje od zidova Levo|Desno
        if (isLeftWall() || isRightWall()) {
            changeDX(); // Menja pravac loptice po X osi
        }
        
        // Prolaz ispod plocice igraca
        if (isUpperLimit() || isBottomLimit()) {
            
            // Poen za Dolnjeg igraca
            if(isUpperLimit()) {
                if(playerUp.isComputer())
                    GameDB.increasePlayerScore();
                int value = Integer.parseInt(statusPlayer1CounterLabel.getText());
                String newValue = String.valueOf(value + 1);
                statusPlayer1CounterLabel.setText(newValue);
            }
            
            // Poen za Gornjeg igraca
            if(isBottomLimit()) {
                if(playerUp.isComputer())
                    GameDB.increaseComputerScore();
                int value = Integer.parseInt(statusPlayer2CounterLabel.getText());
                String newValue = String.valueOf(value + 1);
                statusPlayer2CounterLabel.setText(newValue);
            }
            
            // Zaustavlja igru i postavlja pocetne pozicije
            GameController.setGameStarted(false);
            setAllStrtingPositions();
        }
        
        // Odbijanje od plocice igraca
        if(isUpPlayerX() && isUpPlayerY() || isDownPlayerX() && isDownPlayerY()) {
            
            // postavi spin gornjeg igraca
            if(isUpPlayerX() && isUpPlayerY())
                spin = playerUp.getSpin();
            
            // postavi spin dolnjeg igraca
            if(isDownPlayerX() && isDownPlayerY())
                spin = playerDown.getSpin();
            
            changeDY(); // Menja pravac loptice po Y osi
        }
        
        // Pomera lopticu za dx dy
        adjustBall();
    }
    
    /**
     * Pokrece animaciju prvi put, dakle poziva se samo na pocetku igre
     * 
     * @return void
     */
    public final void play() {
        animation.setRate(0.2);
        animation.play();
        welcomeLabel.setVisible(false);
        startTimeForCycle = new Date().getTime();
    }

    /**
     * Zaustavlja animaciju i postavlja pocetne pozicije
     * 
     * @return void
     */
    public final void stop() {
        animation.stop();
        setStartingPositionForBall();
    }
    
    /**
     * Postavlja pocetne pozicije za igrace, lopticu i podesava velicinu
     * pomeraja loptice
     * 
     * @return void
     */
    public void setAllStrtingPositions() {
        playerDown.setStartingPosition();
        playerUp.setStartingPosition();
        setStartingPositionForBall();
        dx = 1;
        dy = -1;
//        getChildren().add(ball.getCircle()); // Postavljanje lopte u okno
    }
    
    /**
     * Postavlja pocetnu poziciju za lopticu, koja se nalazi na sredini ploce
     * dolnjeg igraca
     * 
     * @return void
     */
    public void setStartingPositionForBall() {
        ball.setX(GameRegistry.getInt("GAME_PANE_WIDTH")/2);
        ball.setY(GameRegistry.getInt("WINDOW_HEIGHT") - playerDown.getHeight() - ball.getRadius());
    }

    /**
     * Uvecava brzinu animacije za datu vrednost.
     * 
     * @param value Brzina animacije
     */
    public final void increaseSpeed(double value) {
        animation.setRate(animation.getRate() + value);
    }

    /**
     * Uvecava brzinu animacije za 0.01
     * 
     * @return void
     */
    public final void increaseSpeed() {
        animation.setRate(animation.getRate() + 0.01);
    }

    /**
     * Usporava brzinu animacije za datu vrednost.
     * 
     * @param value Brzina animacije
     */
    public final void decreaseSpeed(double value) {
        animation.setRate(
                animation.getRate() > 0 ? animation.getRate() - value : 0
        );
    }

    /**
     * Usporava brzinu animacije za 0.01
     * 
     * @return void
     */
    public final void decreaseSpeed() {
        animation.setRate(
                animation.getRate() > 0 ? animation.getRate() - 0.1 : 0
        );
    }
    
    /**
     * Pomera loptu za velicninu pomeraja dx dy
     * 
     * @return void
     */
    protected void adjustBall() {
        // Adjust ball position
        ball.setX(ball.getX() + dx * 1/spin);
        ball.setY(ball.getY() + dy * spin);
    }
    
    /**
     * Proverava dali je lopta dosla do levog zida
     * 
     * @return Boolean
     */
    protected boolean isLeftWall() {
        return ball.getX() < ball.getRadius();
    }
    
    /**
     * Proverava dali je lopta dosla do desnog zida
     * 
     * @return Boolean
     */
    protected boolean isRightWall() {
        return ball.getX() > GameRegistry.getInt("GAME_PANE_WIDTH") - ball.getRadius();
    }
    
    /**
     * Proverava dali je lopta prosla iznad gornjeg igraca
     * 
     * @return Boolean
     */
    protected boolean isUpperLimit() {
        return ball.getY() + 150 < 1;
    }
    
    
    /**
     * Proverava dali je lopta proslaj ispod dolnjeg igraca
     * 
     * @return Boolean
     */
    protected boolean isBottomLimit() {
        return ball.getY() - 150 > GameRegistry.getInt("GAME_PANE_HEIGHT");
    }

    /**
     * Vraca dolnjeg igraca
     * 
     * @return Player objekeat
     */
    public Player getPlayerDown() {
        return playerDown;
    }

    /**
     * Vraca gornjeg igraca
     * 
     * @return Player objekeat
     */
    public Player getPlayerUp() {
        return playerUp;
    }

    /**
     * Vraca pomeraj dx
     * 
     * @return double dx pomeraj
     */
    public double getDX() {
        return dx;
    }

    /**
     * Vraca pomeraj dy
     * 
     * @return double dy pomeraj
     */
    public double getDY() {
        return dy;
    }

    /**
     * Vraca spin loptice
     * 
     * @return double spin loptice
     */
    public double getSpin() {
        return spin;
    }

    /**
     * Postavlja dolnjeg igraca
     * 
     * @param playerDown Dolnji igrac
     */
    protected void setPlayerDown(Player playerDown) {
        this.playerDown = playerDown;
    }

    /**
     * Postavlja gornjeg igraca
     * 
     * @param playerDown Gornji igrac
     */
    protected void setPlayerUp(Player playerUp) {
        this.playerUp = playerUp;
    }

    /**
     * Postavlja pomerja dx
     * 
     * @param dx Pomeraj po x osi
     */
    protected void setDX(double dx) {
        this.dx = dx;
    }

    /**
     * Postavlja pomeraj dy
     * 
     * @param dy Pomeraj po y osi
     */
    protected void setDY(double dy) {
        this.dy = dy;
    }

    /**
     * Postavlja spin loptice
     * 
     * @param spin Spin loptice
     */
    protected void setSpin(double spin) {
        this.spin = spin;
    }
    
    /**
     * Dali je loptica nalazi izmedju krajeva plocice gornjeg igraca, tj po x osi
     * 
     * @return Boolean
     */
    protected boolean isUpPlayerX() {
        return playerUp.getX() <= ball.getX() && playerUp.getX() + playerUp.getWidht() >= ball.getX();
    }
    
    /**
     * Dali je loptica nalazi izmedju pornje i dolnje povrsine gornjeg igraca, tj po y osi
     * 
     * @return Boolean
     */
    protected boolean isUpPlayerY() {
        // ako je imedju povrsine igraca(ploce) i 5px iznad
        return playerUp.getY() + playerUp.getHeight() >= ball.getY()
                && playerUp.getY() + playerUp.getHeight() - 5 <= ball.getY();
    }
    
    /**
     * Dali je loptica nalazi izmedju krajeva plocice dolnjeg igraca, tj po x osi
     * 
     * @return Boolean
     */
    protected boolean isDownPlayerX() {
        // ako je imedju povrsine igraca(ploce) i 5px ispod
        return playerDown.getX() <= ball.getX() && playerDown.getX() + playerDown.getWidht() >= ball.getX();
    }
    
    /**
     * Dali je loptica nalazi izmedju pornje i dolnje povrsine dolnjeg igraca, tj po y osi
     * 
     * @return Boolean
     */
    protected boolean isDownPlayerY() {
        return playerDown.getY() <= ball.getY()
                && playerDown.getY() + 5 >= ball.getY();
    }

    /**
     * Vraca objekat animacije
     * 
     * @return Timeline objekat
     */
    public Timeline getAnimation() {
        return animation;
    }

    /**
     * Postavlja pocetno vreme ciklusa
     * 
     * @return long vreme pocetka
     */
    public long getStartTimeForCycle() {
        return startTimeForCycle;
    }

    /**
     * Vraca interval pre ubranja
     * 
     * @return Interval int
     */
    public int getIntervalBeforeSpeedUp() {
        return intervalBeforeSpeedUp;
    }

    /**
     * Postavlja pocetno vreme ciklusa
     * 
     * @param startTimeForCycle Pocetno vreme
     */
    protected void setStartTimeForCycle(long startTimeForCycle) {
        this.startTimeForCycle = startTimeForCycle;
    }
    
    /**
     * Promena prava po dx osi
     * 
     * @return void
     */
    public void changeDX() {
        dx *= -1;
    }
    
    /**
     * Promena prava po dy osi
     * 
     * @return void
     */
    public void changeDY() {
        dy *= -1;
        updateLastBouce(); // promena pravca po y je odbijanje od igraca, i snima se zadnje
    }
    
    /**
     * Snima poslednje odbijanje loptice i stampla sistemsu poruku u konzoli
     * 
     * @return void
     */
    private void updateLastBouce() {
        lastBouceDX = dx;
        lastBouceDY = dy;
        lastBouceSpin = spin;
        lastBouceBallX = ball.getX();
        lastBouceBallY = ball.getY();
        System.out.println("LastBounce-> " + lastBouceDX + " : " + lastBouceDY
                + " : " + lastBouceSpin + " : " + lastBouceBallX + " : " + lastBouceBallY);
    }

    /**
     * Vraca Welcome labelu sa pozdravnom porukom
     * 
     * @return Label objekat
     */
    public Label getWelcomeLabel() {
        return welcomeLabel;
    }

    /**
     * Vraca objekat lopte za ovu igru
     * 
     * @return Ball objekat
     */
    public Ball getBall() {
        return ball;
    }

    /**
     * Vraca zadnje odbijanje u x koordinati
     * 
     * @return duble vrednost
     */
    public double getLastBouceBallX() {
        return lastBouceBallX;
    }

    /**
     * Vraca zadnje odbijanje u y koordinati
     * 
     * @return duble vrednost
     */
    public double getLastBouceBallY() {
        return lastBouceBallY;
    }

    /**
     * Vraca zadnji spin loptice
     * 
     * @return duble vrednost
     */
    public double getLastBouceSpin() {
        return lastBouceSpin;
    }

    /**
     * Vraca zadnji dx pomeraj
     * 
     * @return duble vrednost
     */
    public double getLastBouceDX() {
        return lastBouceDX;
    }

    /**
     * Vraca zadnji dy pomeraj
     * 
     * @return duble vrednost
     */
    public double getLastBouceDY() {
        return lastBouceDY;
    }

    /**
     * Azurira status igre
     * 
     * @return void
     */
    public void updateStatusPane() {
//        statusPane.add(new Label(playerDown.getName()), 0, 0);
//        statusPane.add(new Label(playerUp.getName()), 0, 1);
//        statusPlayer1Label = new Label(playerDown.getName());
//        statusPlayer2Label = new Label(playerUp.getName());
//        statusPlayer1Label.setText(playerDown.getName());
//        statusPlayer2Label.setText(playerUp.getName());
    }
    
    
}
