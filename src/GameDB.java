
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * Da bi ukljucio driver za mysql, odi na Projects izaberi ovaj projekat desni klik
 * na Libraries i ubaci drajver .jer fajl
 * 
 * Kreiranje table u bazi BounceBall
 * 
 * create table PlayerVsComputer (
 *      id int(10) not null primary key AUTO_INCREMENT,
 *      playerScore int(10),
 *      computerScore int(10)
 * );
 * 
 */

/**
 * Klasa za vezu sa bazom podataka.
 * Radi u rezimu Singleplayer.
 * 
 * Singleton klasa.
 *
 * @author marko
 */
class GameDB {
    private static GameDB instance;
    private static boolean isConnected = false;
    private Connection dbCon = null;
    private final String url = "jdbc:mysql://localhost:3306/BounceBall";
    private final String user = "marko";
    private final String password = "marko";
    private Statement statement;
    
    private GameDB() {
        try {
            isConnected = true;
            Class.forName("com.mysql.jdbc.Driver");
            dbCon = DriverManager.getConnection(url, user, password);
            statement = (Statement) dbCon.createStatement();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            isConnected = false;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            isConnected = false;
        }
    }
    
    /**
     * Uvecava skor igraca Player za 1
     * 
     * @return void
     */
    public static void increasePlayerScore() {
        if(isConnected()) {
            try {
                String query = "select * from PlayerVsComputer where id=1";
                ResultSet result = instance.statement.executeQuery(query);
                if(result.next()) {
                    int number = result.getInt("playerScore");

                    instance.statement.execute("UPDATE PlayerVsComputer "
                        + "SET playerScore=" + ++number + " "
                        + "WHERE id=1;");
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Umanjuje skor igraca Player za 1
     * 
     * @return void
     */
    public static void decreasePlayerScore() {
        if(isConnected()) {
            try {
                String query = "select * from PlayerVsComputer where id=1";
                ResultSet result = instance.statement.executeQuery(query);
                if(result.next()) {
                    int number = result.getInt("playerScore");

                    instance.statement.execute("UPDATE PlayerVsComputer "
                        + "SET playerScore=" + --number + " "
                        + "WHERE id=1;");
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Uvecava skor igraca Computer za 1
     * 
     * @return void
     */
    public static void increaseComputerScore() {
        if(isConnected()) {
            try {
                String query = "select * from PlayerVsComputer where id=1";
                ResultSet result = instance.statement.executeQuery(query);
                if(result.next()) {
                    int number = result.getInt("computerScore");

                    instance.statement.execute("UPDATE PlayerVsComputer "
                        + "SET computerScore=" + ++number + " "
                        + "WHERE id=1;");
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Umanjuje skor igraca Computer za 1
     * 
     * @return void
     */
    public static void decreaseComputerScore() {
        if(isConnected()) {
            try {
                String query = "select * from PlayerVsComputer where id=1";
                ResultSet result = instance.statement.executeQuery(query);
                if(result.next()) {
                    int number = result.getInt("computerScore");

                    instance.statement.execute("UPDATE PlayerVsComputer "
                        + "SET computerScore=" + --number + " "
                        + "WHERE id=1;");
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Vraca skor igraca Pleyer
     * 
     * @return Skor igraca
     */
    public static int getPlayerScore() {
        if(isConnected()) {
            try {
                String query = "select * from PlayerVsComputer where id=1";
                ResultSet result = instance.statement.executeQuery(query);
                if(result.next())
                    return result.getInt("playerScore");
                
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        
        return 0;
    }
    
    /**
     * Vraca skor igraca Computer
     * 
     * @return Skor masine
     */
    public static int getComputerScore() {
        if(isConnected()) {
            try {
                String query = "select * from PlayerVsComputer where id=1";
                ResultSet result = instance.statement.executeQuery(query);
                if(result.next())
                    return result.getInt("computerScore");
                
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        
        return 0;
    }

    /**
     * Inicijalizuje unutrasnji objekat.
     * 
     * @return void
     */
    public static void create() {
        if(instance == null)
            instance = new GameDB();
    }
    
    /**
     * Zatvara konekciju sa bazom.
     * 
     * @return void
     */
    public static void closeConncetion() {
        if(instance != null && isConnected()) {
            try {
                    instance.dbCon.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Proverava dali je uspostavljena veza.
     * 
     * @return boolean
     */
    public static boolean isConnected() {
        return isConnected;
    }
    
    
}
