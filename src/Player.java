
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Klasa za definisanje igraca. Igrac je na platnu predstavljen u vidu plocice
 * koja predstavlja objekat tipa Rectangle. Pored graficke prezentacije sadrzi i
 * njegov polozaj GORE/DOLE, vrstu IGRAC/MASINA, i ime igraca.
 *
 * @author Marko
 */
class Player {
    public static final boolean UP = false;
    public static final boolean DOWN = true;
    
    private BallPane pane = null;
    private final Rectangle rectangle = new Rectangle();
    private final Color color = Color.DARKGREY;
    private String name;
    private final boolean isDown;
    private boolean isComputer = false;
    private double spin = 1;
    
    public Player(boolean isDown) {
        this.name = "Computer";
        this.isDown = isDown;
        isComputer = true;
        setDefault();
    }

    public Player(String name, boolean isDown) {
        this.name = name;
        this.isDown = isDown;
        setDefault();
    }
    
    /**
     * Postavlja velicinu Rectangle objekta i stavlja ga na pocetni polozaj.
     * 
     * @return void
     */
    private void setDefault() {
        rectangle.setWidth(GameRegistry.getInt("PLAYER_WIDTH"));
        rectangle.setHeight(GameRegistry.getInt("PLAYER_HEIGHT"));
        rectangle.setFill(color);
        setStartingPosition();
    }
    
    /**
     * Postavlja igraca na pocetni polozaj.
     * 
     * @return void
     */
    public void setStartingPosition() {
        rectangle.setX(GameRegistry.getInt("GAME_PANE_WIDTH")/2 - rectangle.getWidth()/2);
        
        if(isUP())
            rectangle.setY(0);
        else
            rectangle.setY(GameRegistry.getInt("WINDOW_HEIGHT") - rectangle.getHeight());
    }
    
    /**
     * Pokrece komputer igraca, u osnovi prati polozaj loptice, tako da je nepobediv
     * za sada.
     * 
     * @return void
     */
    public void computerMoveUpdate() {
        if(isComputer())
            if(getPane().getBall().getX() > getWidht()/2 
                    && getPane().getBall().getX() < GameRegistry.getInt("GAME_PANE_WIDTH") - getWidht()/2 )
                setX(getPane().getBall().getX() - getWidht()/2);
    }

    /**
     * Vraca sirinu plocice igraca
     * 
     * @return Sirina plocice
     */
    public double getWidht() {
        return rectangle.getWidth();
    }

    /**
     * Vraca visina plocice igraca
     * 
     * @return Visina plocice
     */
    public double getHeight() {
        return rectangle.getHeight();
    }

    /**
     * Vraca ime igraca
     * 
     * @return String ime
     */
    public String getName() {
        return name;
    }

    /**
     * Proverava dali igrac ima dolnju pozicju
     * 
     * @return boolean
     */
    public boolean isDown() {
        return isDown;
    }

    /**
     * Proverava dali igrac ima gornju pozicju
     * 
     * @return boolean
     */
    public boolean isUP() {
        return !isDown;
    }

    /**
     * Vraca x koodinatu plocice igraca
     * 
     * @return Kooridnata x
     */
    public double getX() {
        return rectangle.getX();
    }

    /**
     * Vraca y koodinatu plocice igraca
     * 
     * @return Kooridnata y
     */
    public double getY() {
        return rectangle.getY();
    }

    /**
     * Postavlja x koodrinatu igraca
     * 
     * @param x x koordinata
     */
    public void setX(double x) {
        rectangle.setX(x);
        
        if(!GameController.isGameStarted() && this.isDown())
                    moveBallOnTheStart(x);
//        rectangle.relocate(x, rectangle.getY());
    }
    
    /**
     * Pomera lopticu platna po pomeraju igraca
     * 
     * @param x Promena x koodrinate plocice
     */
    public void moveBallOnTheStart(double x) {
        getPane().getBall().setX(x+this.getWidht()/2);
        
    }

    /**
     * Postavlja y koodrinatu igraca
     * 
     * @param y y koordinata
     */
    public void setY(double y) {
        rectangle.setY(y);
    }

    /**
     * Vraca objekat Rectangle
     * 
     * @return Rectangle
     */
    public Rectangle getRectangle() {
        return rectangle;
    }

    /**
     * Proverava dali je igrac masina
     * 
     * @return boolean
     */
    public boolean isComputer() {
        return isComputer;
    }

    /**
     * Vraca platno igre igraca
     * 
     * @return BallPane
     */
    public BallPane getPane() {
        return pane;
    }

    /**
     * Postavlja platno igre igraca
     * 
     * @param pane 
     */
    public void setPane(BallPane pane) {
        this.pane = pane;
    }

    /**
     * Postavlja ime igraca
     * 
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Generise spin masinskog igraca po random principu
     * 
     * @return void
     */
    public void calculateComputerSpin() {
        spin = Math.random()/2 + 0.8;
    }
    
    /**
     * Vraca spin potice za igraca
     * 
     * @return spin
     */
    public double getSpin() {
        if(isComputer)
            calculateComputerSpin();
        return spin;
    }

    /**
     * Postavlja spin igraca
     * 
     * @param spin 
     */
    public void setSpin(double spin) {
        this.spin = spin;
    }
    
}
