
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.Thread.State;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * Serverski deo mrezne aplikacije, sluzi za kreiraje servera koji prihvata 2 klijenta, 
 * u sebi sadrzi 3 niti:
 *      -prva- kreiranje servera, povezivanje igraca i inicijalizacija ostalih niti
 *      -druga- prima podatke sa igraca 1 i salje igacu 2
 *      -treca- prima podatke sa igraca 2 i salje igacu 1
 * 
 * Implementirana je u vidu Singleton sablona.
 * 
 * @author marko
 */
class GameServer {
    private static GameServer instance;
    private static boolean STATUS = false;
    private static boolean AWATING_PLAYERS = false;
    private String nickForPlayer1 = "Nick";
    private String nickForPlayer2 = "Nick";
    private Thread initThread;
    private Thread fromPlayer1ToPlayer2Thread;
    private Thread fromPlayer2ToPlayer1Thread;
    
    /**
     * Inicijalizacija Singleton objekta
     * 
     * @return void
     */
    public static void create() {
        if(instance == null)
            instance = new GameServer();
    }
    
    private GameServer() {}
    
    /**
     * Pokusava da pokrene server
     * 
     * @param port 
     */
    public static void start(int port) {
        create();
        stop();
        run(port);
    }
    
    /**
     * Klasa za pokretanje servera
     * 
     * @param port Port na kome ce server biti pokrenut
     */
    private static void run(int port) {
        // Ako server ceka igrace, nema poente praviti novi jer ce ih primiti stari
        if(GameServer.AWATING_PLAYERS) return;
        
        // Uslov za komunikaciju izmednju klijenata
        GameServer.STATUS = true;
        
        // Samostalna nit servera, radi dok se igraci ne povezu, uspostavlja 2 niti za komunikaciju
        // izmedju klijenata i gasi se
        instance.initThread = new Thread(()-> {
            try {
                
                System.out.println("SERVER: Server se pokrece");
                
                ServerSocket serverSocet = new ServerSocket(port);
                
                System.out.println("SERVER: Server started at " + new Date() + " on port " + port);
                System.out.println("SERVER: Wait for Player1 to join");
                
                GameServer.AWATING_PLAYERS = true;
                
                // Ceka igraca Player1
                Socket player1 = serverSocet.accept();
                System.out.println("SERVER: Player1 je povezan");
                DataInputStream inputFromPlayer1 = new DataInputStream(player1.getInputStream());
                DataOutputStream outputToPlayer1 = new DataOutputStream(player1.getOutputStream());
                outputToPlayer1.writeInt(1);
                instance.nickForPlayer1 = inputFromPlayer1.readUTF();
                GameController.getServerPane().setPlayer1Nick(instance.nickForPlayer1);
                System.out.println("SERVER: Player1(" + instance.nickForPlayer1 + ") je uspesno povezan");
                
                System.out.println("SERVER: Player1 joined at " + new Date());
                System.out.println("SERVER: Wait for Player2 to join");
                
                // Ceka igraca Player2
                Socket player2 = serverSocet.accept();
                System.out.println("SERVER: Player2 je povezan");
                DataInputStream inputFromPlayer2 = new DataInputStream(player2.getInputStream());
                DataOutputStream outputToPlayer2 = new DataOutputStream(player2.getOutputStream());
                outputToPlayer2.writeInt(2);
                instance.nickForPlayer2 = inputFromPlayer2.readUTF();
                GameController.getServerPane().setPlayer2Nick(instance.nickForPlayer2);
                System.out.println("SERVER: Player2(" + instance.nickForPlayer2 + ") je uspesno povezan");
                
                System.out.println("SERVER: Player2 joined at " + new Date());
                
                outputToPlayer1.writeUTF(instance.nickForPlayer2);
                outputToPlayer2.writeUTF(instance.nickForPlayer1);
                
                GameServer.AWATING_PLAYERS = false;
                
                // Nova nit za samostalno slanje sa Player1->Player2
                instance.fromPlayer1ToPlayer2Thread = new Thread(()->{
                    while (GameServer.STATUS)
                        try {
                            String fromPlayer1 = inputFromPlayer1.readUTF();
                            outputToPlayer2.writeUTF(fromPlayer1);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                                
                        }
                });
                
                instance.fromPlayer1ToPlayer2Thread.start();
                
                // Nova nit za samostalno slanje sa Player2->Player1
                instance.fromPlayer2ToPlayer1Thread = new Thread(()->{
                    while (GameServer.STATUS)
                        try {
                            String fromPlayer2 = inputFromPlayer2.readUTF();
                            outputToPlayer1.writeUTF(fromPlayer2);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                                
                        }
                });
                
                instance.fromPlayer2ToPlayer1Thread.start();
                
                System.out.println("SERVER: Server sa igracima i nitima je kreiran u " + new Date());
                
            } catch (IOException ex) {
                ex.printStackTrace();
                                
            }
        });
        
        instance.initThread.start();        
    }
    
    /**
     * Zaustavlja server i sve instance u njemu.
     * 
     * @return void
     */
    public static void stop() {
        create();
        
        // Ako server ceka igrace, nema poente gasiti ga, jer ovako neces prekinuti nit
        if(GameServer.AWATING_PLAYERS) return;
        
        // Prekida while cikluse za komunikaciju igraca
        GameServer.STATUS = false;
        
        // Pokusava da ugasi sve niti
        if(instance.initThread != null
                && instance.fromPlayer1ToPlayer2Thread != null
                && instance.fromPlayer2ToPlayer1Thread != null) {
            
            if(instance.initThread.getState() != State.TERMINATED)
                instance.initThread.interrupt();

            if(instance.fromPlayer1ToPlayer2Thread.getState() != State.TERMINATED)
                instance.fromPlayer1ToPlayer2Thread.interrupt();

            if(instance.fromPlayer2ToPlayer1Thread.getState() != State.TERMINATED)
                instance.fromPlayer2ToPlayer1Thread.interrupt();
        }
    }
    
    /**
     * Dobijanje statusa servera.
     * 
     * @return Status 
     */
    public static boolean getStatus() {
        return STATUS;
    }

    /**
     * Dobijanje niti za inicijalizaciju
     * 
     * @return Thread
     */
    public static Thread getInitThread() {
        return instance.initThread;
    }

    /**
     * Dobijanje niti za primanje podataka sa prvog klijenta i slanje drugom.
     * 
     * @return Thread
     */
    public static Thread getFromPlayer1ToPlayer2Thread() {
        return instance.fromPlayer1ToPlayer2Thread;
    }

    /**
     * Dobijanje niti za primanje podataka sa drugog klijenta i slanje prvom.
     * 
     * @return Thread
     */
    public static Thread getFromPlayer2ToPlayer1Thread() {
        return instance.fromPlayer2ToPlayer1Thread;
    }

    /**
     * Vraca nik igraca 1.
     * 
     * @return String Nik igraca 1
     */
    public static String getNickForPlayer1() {
        return instance.nickForPlayer1;
    }

    /**
     * Vraca nik igraca 2.
     * 
     * @return String Nik igraca 2
     */
    public static String getNickForPlayer2() {
        return instance.nickForPlayer2;
    }
    
    
}
