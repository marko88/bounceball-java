import javafx.application.Application;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Osnovna klasa aplikacije/igre, sluzi za inicijalizaciju igre i upravlja
 * promenama platna
 *
 * @author Marko
 */
public class BounceBall extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        // Podesacanje prozora i igraca
        GameRegistry.create();
        GameRegistry.set("WINDOW_WIDTH", "700");
        GameRegistry.set("WINDOW_HEIGHT", "500");
        GameRegistry.set("GAME_PANE_WIDTH", "500");
        GameRegistry.set("GAME_PANE_HEIGHT", "500");
        GameRegistry.set("PLAYER_WIDTH", "100");
        GameRegistry.set("PLAYER_HEIGHT", "8");
        
        GameDB.create();
        GameController.create();
        GameMenu.create();
        
        
        primaryStage.setScene(GameMenu.getMainMenuScene());
        
        // PROMENA SCENE onMouseAction
        
        // Game Main menu
        GameMenu.getMainMenuPane().getSinglePlayerLabel().setOnMousePressed(e -> {
            GameController.setGame(GameTypes.SinglePlayerGame);
            primaryStage.setScene(GameController.getSinglePlayerScene());
            GameController.getSinglePlayerPane().requestFocus();
            GameController.getSinglePlayerPane().getWelcomeLabel().setVisible(true);
        });
        GameMenu.getMainMenuPane().getMultiPlayerLabel().setOnMousePressed(e -> {
            primaryStage.setScene(GameMenu.getMultiPlayerMenuScene());
        });
        GameMenu.getMainMenuPane().getExitLabel().setOnMousePressed(e -> {
            exitApplication();
        });
        
        // Game Multiplayer menu
        GameMenu.getMultiPlayerMenuPane().getCreateServerLabe().setOnMousePressed(e -> {
            primaryStage.setScene(GameMenu.getCreateServerMenuScene());
        });
        GameMenu.getMultiPlayerMenuPane().getJoinLabel().setOnMousePressed(e -> {
            primaryStage.setScene(GameMenu.getJoinMenuScene());
        });
        GameMenu.getMultiPlayerMenuPane().getBackLabel().setOnMousePressed(e -> {
            primaryStage.setScene(GameMenu.getMainMenuScene());
        });
        
        // Create Server Multiplayer menu
        GameMenu.getCreateServerMenuPane().getCreateJoinLabel().setOnMousePressed(e -> {
            GameController.setGame(GameTypes.ServerGame);
            primaryStage.setScene(GameController.getServerScene());
        });
        GameMenu.getCreateServerMenuPane().getBackLabel().setOnMousePressed(e -> {
            primaryStage.setScene(GameMenu.getMultiPlayerMenuScene());
        });
        
        // Join Multiplayer menu
        GameMenu.getJoinMenuPane().getCreateJoinLabel().setOnMousePressed(e -> {
            GameController.setGame(GameTypes.ClientGame);
            primaryStage.setScene(GameController.getMultiPlayerScene());
            GameController.getMultiPlayerPane().requestFocus();
            GameController.getMultiPlayerPane().getWelcomeLabel().setVisible(true);
        });
        GameMenu.getJoinMenuPane().getBackLabel().setOnMousePressed(e -> {
            primaryStage.setScene(GameMenu.getMultiPlayerMenuScene());
        });
        
        // Single-Player
        GameController.getSinglePlayerPane().setOnKeyReleased(en -> {
            if (en.getCode() == KeyCode.ESCAPE)
                primaryStage.setScene(GameMenu.getMainMenuScene());
        });
        
        // Multi-Player
        GameController.getMultiPlayerPane().setOnKeyReleased(en -> {
            if (en.getCode() == KeyCode.ESCAPE) {
                primaryStage.setScene(GameMenu.getMainMenuScene());
                GameServer.stop();
                GameClient.stop();
            }
        });
        
        // Server Exit Label
        GameController.getServerPane().getExitLabel().setOnMousePressed(e -> {
            exitApplication();
        });
        
        primaryStage.setTitle("Bounce Ball");
        primaryStage.show();
        primaryStage.setResizable(false);
        
        // On Close shutdown threds
        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            exitApplication();
        });
        
    }
    
    /**
     * Zatvara vezu sa bazom podataka i gasi sve pokernute niti.
     * 
     */
    public void exitApplication() {
        GameDB.closeConncetion();
        Runtime.getRuntime().halt(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
