
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * Klasa Ball definise loptu u platnu, predstavljena je u vidu Circle objekta.
 * Sadrzi metode za podesavanje boje, radijusa i polozaja po X/Y koodrinatama.
 *
 * @author Marko
 */
class Ball {
    
    private Color color;
    private double radius;
    private final Circle circle;
    
    public Ball() {
        radius = 5;
        color = Color.LIGHTGREY;
        circle = new Circle(radius, radius, radius);
        circle.setFill(color);
    }
    
    /**
     * Metoda za dobijanje objekata Circle.
     * 
     * @return Circle objekat
     */
    public Circle getCircle() {
        return circle;
    }
    
    /**
     * Postavlja radius lopte.
     * 
     * @param radius Radius lopte u px
     */
    public void setRadius(double radius) {
        this.radius = radius;
        circle.setRadius(radius);
    }
    
    /**
     * Vraca radijus lopte.
     * 
     * @return Radius lopte 
     */
    public double getRadius() {
        return radius;
    }
    
    /**
     * Postavlja boju lopte.
     * 
     * @param color Objekat Color za definisanje boje loptice
     */
    public void setColor(Color color) {
        this.color = color;
        circle.setFill(color);
    }
    
    /**
     * Vraca X koodrinatu loptice.
     * 
     * @return X koodrinata loptice
     */
    public double getX() {
        return circle.getCenterX();
    }
    
    /**
     * Vraaca y koodriantu loptice.
     * 
     * @return Y koordinata loptice 
     */
    public double getY() {
        return circle.getCenterY();
    }
    
    /**
     * Postavlja lopticu u X koordinati.
     * 
     * @param x Koordinata X
     */
    public void setX(double x) {
        circle.setCenterX(x);
    }
    
    /**
     * Postavlja lopticu u Y koordinati.
     * 
     * @param y Koodrinata Y
     */
    public void setY(double y) {
        circle.setCenterY(y);
    }
}