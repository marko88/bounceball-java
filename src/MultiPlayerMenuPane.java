
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Meni za Multiplayer.
 * Pravi platno menija.
 * 
 * @author marko
 */
class MultiPlayerMenuPane extends VBox {
    private final Label createServerLabe;
    private final Label joinLabel;
    private final Label backLabel;

    public MultiPlayerMenuPane() {
        setWidth(GameRegistry.getInt("WINDOW_WIDTH"));
        setHeight(GameRegistry.getInt("WINDOW_HEIGHT"));
        setPrefSize(GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        setStyle("-fx-background-color: #000;");
        
        createServerLabe = new Label("Create Server");
        joinLabel = new Label("Join");
        backLabel = new Label("Back");
        
        createServerLabe.setPadding(new Insets(10));
        joinLabel.setPadding(new Insets(10));
        backLabel.setPadding(new Insets(10));
        
        createServerLabe.setTextFill(Color.DARKGREY);
        joinLabel.setTextFill(Color.DARKGREY);
        backLabel.setTextFill(Color.DARKGREY);
        
        createServerLabe.setFont(new Font(20));
        joinLabel.setFont(new Font(20));
        backLabel.setFont(new Font(20));
        
        setAlignment(Pos.CENTER);
        
        getChildren().addAll(createServerLabe, joinLabel, backLabel); // Postavljanje menija
    }

    /**
     * Vraca lebalu za Create Server.
     * 
     * @return Label
     */
    public Label getCreateServerLabe() {
        return createServerLabe;
    }

    /**
     * Vraca lebalu za Join.
     * 
     * @return Label
     */
    public Label getJoinLabel() {
        return joinLabel;
    }

    /**
     * Vraca lebalu za Back.
     * 
     * @return Label
     */
    public Label getBackLabel() {
        return backLabel;
    }
    
}
