
import java.util.Date;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Platno servera. Daje inforamcije o statusu servera.
 *
 * @author marko
 */
class ServerPane extends VBox {
    private final Label exitLabel;
    
    private final TextArea statusTextArea;

    public ServerPane() {
        setWidth(GameRegistry.getInt("WINDOW_WIDTH"));
        setHeight(GameRegistry.getInt("WINDOW_HEIGHT"));
        setPrefSize(GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        setStyle("-fx-background-color: #000;");
        
        exitLabel = new Label("Exit");
        statusTextArea = new TextArea("Server started at " + new Date());
        statusTextArea.setEditable(false);
        statusTextArea.setMaxSize(400, 200);
        statusTextArea.setOpacity(0.8);
        
        exitLabel.setPadding(new Insets(10));
        
        exitLabel.setTextFill(Color.DARKGREY);
        
        exitLabel.setFont(new Font(20));
        
        setAlignment(Pos.CENTER);
        
        getChildren().addAll(statusTextArea, exitLabel); // Postavljanje menija
        
        // Podesava osvetljenje dugmeta Exit
        exitLabel.setOnMouseMoved(e -> {
            exitLabel.setTextFill(Color.WHITE);
        });
        statusTextArea.setOnMouseMoved(e -> {
            exitLabel.setTextFill(Color.DARKGREY);
        });
    }

    /**
     * Vraca Exit labelu
     * 
     * @return Label
     */
    public Label getExitLabel() {
        return exitLabel;
    }
    
    /**
     * Postavlja nick igraca 1
     * 
     * @param nick 
     */
    public void setPlayer1Nick(String nick) {
        statusTextArea.setText(statusTextArea.getText() + "\n" + "Player 1: " + nick + " is connected on " + new Date());
    }
    
    /**
     * Postavlja nick igraca 2
     * 
     * @param nick 
     */
    public void setPlayer2Nick(String nick) {
        statusTextArea.setText(statusTextArea.getText() + "\n" + "Player 2: " + nick + " is connected on " + new Date());
    }
    
}
