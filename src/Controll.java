/**
 * Abstraktna klasa za podesavanje kontrole za igraca
 *
 * @author marko
 */
abstract class Controll {
    protected BallPane pane;
    protected Player player;
    
    public Controll(BallPane pane, Player player) {
        this.pane = pane;
        this.player = player;
        setControll();
    }
    
    /**
     * Abstraktna metoda za definisanje kontrola
     * 
     * @return void
     */
    abstract public void setControll();

    /**
     * Vraca platno igraca
     * 
     * @return BallPane
     */
    public BallPane getPane() {
        return pane;
    }

    /**
     * Vraca igraca
     * 
     * @return Player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Postavlja platno igraca
     * 
     * @return void
     */
    public void setPane(BallPane pane) {
        this.pane = pane;
    }

    /**
     * Postavlja igraca
     * 
     * @return void
     */
    public void setPlayer(Player player) {
        this.player = player;
    }
    
    
}
