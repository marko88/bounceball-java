
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

/**
 * Klasa za dodeljivanje kontrole igracu. Definise dogadjaje pomeranja misa i 
 * pritiska tastera, i shodno tome poziva metode za pomeranje igraca na platnu.
 * Takodje sadrzi metode za ukljucivanje i iskljucivanje misa i tastaure i zasebnu
 * nit koja racuna brzinu pomeraja igraca i definise spin loptice.
 *
 * @author Marko
 */
class PlayerControll extends Controll {
    private boolean mouseIsOn = true;
    private boolean keyboardIsOn = true;
    private final int KEY_MOVE = 10;
    private final Thread threadForSpinCount;
    private double lastXForPlayer;

    public PlayerControll(BallPane pane, Player player) {
        super(pane, player);
        
        // Nit za definisanje spina loptice
        threadForSpinCount = new Thread(()->{
            while(true) {
                double x = player.getX();
                double newSpin = 1.0;
                
                // If player is moved, else set default spin
                if(x != lastXForPlayer) {
                    newSpin = x / lastXForPlayer;
//                    System.out.println(newSpin + "\t" + x + "\t" + lastXForPlayer + "\t" + new Date());
                }
                
                if(newSpin == 0)
                    newSpin = 1.0;
                
                player.setSpin(newSpin);
                
                    
                lastXForPlayer = x;
                
                try {
                    Thread.sleep(60);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PlayerControll.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        threadForSpinCount.start();
    }

    /**
     * Postavlja kontrole setOnKeyPressed,setOnMouseMoved i setOnMousePressed 
     * za igraca koji ima kontrolu nad objektom Pleyer
     * 
     * @return void
     */
    @Override
    public void setControll() {
        
        this.pane.setOnKeyPressed(e -> {
            if(keyboardIsOn) {
                if (e.getCode() == KeyCode.LEFT && GameController.isGameStarted()) {
                    movePlayerLeftWithKey();
                }
                else if (e.getCode() == KeyCode.RIGHT && GameController.isGameStarted()) {
                    movePlayerRightWithKey();
                }
                else if (e.getCode() == KeyCode.SPACE && !GameController.isGameStarted()) {
                    GameController.setGameStarted(true);
                }
                else if (e.getCode() == KeyCode.LEFT && !GameController.isGameStarted()) {
                    movePlayerLeftWithKey();
                    if(player.isDown())
                        moveBallLeftWithKey();
                }
                else if (e.getCode() == KeyCode.RIGHT && !GameController.isGameStarted()) {
                    movePlayerRightWithKey();
                    if(player.isDown())
                        moveBallRightWithKey();
                }
            }
        });
        
        this.pane.setOnMouseMoved(e -> {
            if(mouseIsOn)
                movePlayerWithMouse(e.getX());
        });
        
        this.pane.setOnMousePressed(e -> {
            if(mouseIsOn && e.getButton() == MouseButton.PRIMARY 
                    && !GameController.isGameStarted()
                    && getPlayer().isDown())
                GameController.setGameStarted(true);
            
            if(e.getButton() == MouseButton.SECONDARY)
                mouseIsOn = !mouseIsOn;
            
        });
    }
    
    /**
     * Iskljucije kontrole misa
     * 
     * @return void
     */
    public void setMouseControllOff() {
        mouseIsOn = false;
    }
    
    /**
     * Ukljucije kontrole misa
     * 
     * @return void
     */
    public void setMouseControllOn() {
        mouseIsOn = true;
    }
    
    /**
     * Iskljucije kontrole tastature
     * 
     * @return void
     */
    public void setKeyboardControllOff() {
        keyboardIsOn = false;
    }
    
    /**
     * Ukljucije kontrole tastature
     * 
     * @return void
     */
    public void setKeyboardControllOn() {
        keyboardIsOn = true;
    }
    
    /**
     * Iskljucije kontrole
     * 
     * @return void
     */
    public void setControllsOff() {
        setMouseControllOff();
        setKeyboardControllOff();
    }
    
    /**
     * Ukljucije kontrole
     * 
     * @return void
     */
    public void setControllsOn() {
        setMouseControllOn();
        setKeyboardControllOn();
    }
    
    /**
     * Pomera igraca po x koordinati, tj objekat Rectangle
     * 
     * @param x Koodridata x za novi polozaj igraca
     */
    public void movePlayerWithMouse(double x) {
        if(x <= player.getWidht()/2)
            player.setX(0);
        else if(x >= GameRegistry.getInt("GAME_PANE_WIDTH") - player.getWidht()/2)
            player.setX(GameRegistry.getInt("GAME_PANE_WIDTH") - player.getWidht());
        else
            player.setX(x - player.getWidht()/2);
    }
    
    /**
     * Pomera igraca levo, preko tastera.
     * 
     * @return void
     */
    private void movePlayerLeftWithKey() {
        double newPos = player.getX() - KEY_MOVE;
        if(newPos >= 0)
            player.setX(newPos);
        else
            player.setX(0);
    }
    
    /**
     * Pomera igraca desno, preko tastera.
     * 
     * @return void
     */
    private void movePlayerRightWithKey() {
        double newPos = player.getX() + KEY_MOVE;
        if(newPos <= GameRegistry.getInt("GAME_PANE_WIDTH") - player.getWidht())
            player.setX(newPos);
        else
            player.setX(GameRegistry.getInt("GAME_PANE_WIDTH") - player.getWidht());
    }
    
    /**
     * Pomera lopticu levo, preko tastera.
     * 
     * @return void
     */
    private void moveBallLeftWithKey() {
        double newPos = getPane().getBall().getX() - KEY_MOVE;
        if(newPos >= 0 + player.getWidht()/2)
            getPane().getBall().setX(newPos);
        else
            getPane().getBall().setX(0 + player.getWidht()/2);
    }
    
    /**
     * Pomera lopticu desno, preko tastera.
     * 
     * @return void
     */
    private void moveBallRightWithKey() {
        double newPos = getPane().getBall().getX() + KEY_MOVE;
        if(newPos <= GameRegistry.getInt("GAME_PANE_WIDTH") - player.getWidht()/2)
            getPane().getBall().setX(newPos);
        else
            getPane().getBall().setX(GameRegistry.getInt("GAME_PANE_WIDTH") - player.getWidht()/2);
    }
}
