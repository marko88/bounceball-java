/**
 * Klasa za definisanje Singleplayer igre.
 *
 * @author Marko
 */
class SinglePlayerGame extends Game {
    
    @Override
    protected void setup() {
        setPlayerUp( new Player(Player.UP) );
        setPlayerDown( new Player("Player", Player.DOWN) );
        setPane( new BallPane(getPlayerUp(), getPlayerDown()) );
        setPlayerControll( new PlayerControll(getPane(), getPlayerDown()) );
    }

}
