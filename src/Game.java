/**
 * Absraktna klasa za definisanje igre, sluzi kao medjusloj izmedju GameController-a
 * i platna igre BallPane
 *
 * @author marko
 */
abstract class Game {
    private Player playerDown;
    private Player playerUp;
    private BallPane pane;
    private PlayerControll playerControll;
    private boolean gameStarted = false;
    
    abstract protected void setup();

    public Game() {
        setup();
    }
    
    /**
     * Pokrece animaciju na platnu
     * 
     * @return void
     */
    public final void play() {
        pane.play();
    }
    
    /**
     * Zaustavlja animaciju na platnu
     * 
     * @return void
     */
    public final void stop() {
        pane.stop();
    }

    /**
     * Postavlja dolnjeg igraca
     * 
     * @return void
     */
    protected final void setPlayerDown(Player playerDown) {
        this.playerDown = playerDown;
    }

    /**
     * Postavlja gornjeg igraca
     * 
     * @return void
     */
    protected final void setPlayerUp(Player playerUp) {
        this.playerUp = playerUp;
    }
    
    /**
     * Vraca dolnjeg igraca
     * 
     * @return Player
     */
    public final Player getPlayerDown() {
        return playerDown;
    }

    /**
     * Vraca gornjeg igraca
     * 
     * @return Player
     */
    public final Player getPlayerUp() {
        return playerUp;
    }

    /**
     * Vraca platno igre
     * 
     * @return BallPane
     */
    public final BallPane getPane() {
        return pane;
    }

    /**
     * Postavlja platno igre
     * 
     * @param pane PLatno
     */
    protected final void setPane(BallPane pane) {
        this.pane = pane;
    }

    /**
     * Postavlja kontru nad igracem
     * 
     * @param playerControll Controll objekat
     */
    protected final void setPlayerControll(PlayerControll playerControll) {
        this.playerControll = playerControll;
    }

    /**
     * Vraca objekat kontrole igraca
     * 
     * @return PlayerControll
     */
    public final PlayerControll getPlayerControll() {
        return playerControll;
    }
    
    /**
     * Proverava dali je pocela igra
     * 
     * @return boolean
     */
    public boolean isGameStarted() {
        return gameStarted;
    }

    /**
     * Postavlja stanje igre, poceta/zavrsena
     * 
     * @param gameStarted Status igre TRUE/FALSE
     */
    public void setGameStarted(boolean gameStarted) {
        this.gameStarted = gameStarted;
//        pane.setStartingPositionForBall();
        if(gameStarted)
            play();
        else
            stop();
    }
    
}
