import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Klijentski deo mrezne aplikacije, sluzi za povezivanje na server, u sebi
 * sadrzi 4 niti:
 *      -prva- povezizvanje na server i inicijalizacija ostalih niti
 *      -druga- salje poziciju igraca serveru
 *      -treca- prima poziciju suprotnog igraca sa servera
 *      -cetvrta- salje poziciju lopte
 * 
 * Implementirana je u vidu Singleton sablona.
 *
 * @author marko
 */
class GameClient {
    private static GameClient instance;
    private static boolean STATUS = false;
    private Socket socket = null;
    private DataOutputStream toServer = null;
    private DataInputStream fromServer = null;
    private Game game;
    private String nick;
    private String nickForNetworkPlayer;
    private String host;
    private int port;
    private int playerNumber;
    private Thread initThread;
    private Thread sendPlayerPositionThread;
    private Thread reciveOthersPlayerPositionThread;
    private Thread sendBallPositionThread;
    private boolean isStartedSend = false;

    public static void create() {
        if(instance == null)
            instance = new GameClient();
    }
    
    private GameClient() {}
    
    /**
     * Pokrece klijeta
     * 
     * @param multiPlayerGame Game objekat sa multiplayer podesavanjima
     * @param nick Nick igraca
     * @param host Host servera
     * @param port Port servera
     */
    public static void start(Game multiPlayerGame,  String nick, String host, int port) {
        create();
        instance.game = multiPlayerGame;
        instance.nick = nick;
        instance.host = host;
        instance.port = port;
        
        // Uslov za komunikaciju izmednju klijenata
        GameClient.STATUS = true;
        
        // Samostalna nit servera, radi dok se igraci ne povezu, uspostavlja 2 niti za komunikaciju
        // izmedju klijenata i gasi se
        instance.initThread = new Thread(()-> {
            try {
                
                // Povezuje se na server i postavlja ulzano izlazne kanale
                System.out.println("CLIENT: Client(" + nick + ") se povezuje");
                instance.socket = new Socket(instance.host, instance.port);
                instance.fromServer = new DataInputStream(instance.socket.getInputStream());
                instance.toServer = new DataOutputStream(instance.socket.getOutputStream());
                System.out.println("CLIENT: Client(" + nick + ") je povezan na HOST(" + instance.host + ") : PORT(" + instance.port + ") u " + new Date());
                
                // Dobija poziciju od severa 1|2 Down|Up i podesava ime
                instance.playerNumber = instance.fromServer.readInt();
                System.out.println("CLIENT: Client(" + nick + ") ima poziciju " + instance.playerNumber);
                if(instance.playerNumber == 1)
                    instance.game.getPane().getPlayerDown().setName(instance.nick);
                else
                    instance.game.getPane().getPlayerUp().setName(instance.nick);
                instance.game.getPane().updateStatusPane();
                
                // Salje Nick serveru
                instance.toServer.writeUTF(nick);
                
                // Dobija Nick protivnika
                instance.nickForNetworkPlayer = instance.fromServer.readUTF();
                System.out.println("CLIENT: Client(" + nick + ") Protivnik(" + instance.nickForNetworkPlayer + ") je povezan");
                if(instance.playerNumber == 1)
                    instance.game.getPane().getPlayerUp().setName(instance.nickForNetworkPlayer);
                else
                    instance.game.getPane().getPlayerDown().setName(instance.nickForNetworkPlayer);
                
                // Postavi igrace na mesta i dodeli im kontrole
                if(instance.playerNumber == 1) {
                    Player localPlayer = instance.game.getPlayerDown();
                    localPlayer.setName(nick);
                    instance.game.getPlayerUp().setName(instance.nickForNetworkPlayer);
                    instance.game.getPlayerControll().setPlayer(localPlayer);
                } else {
                    Player localPlayer = instance.game.getPlayerUp();
                    localPlayer.setName(nick);
                    instance.game.getPlayerDown().setName(instance.nickForNetworkPlayer);
                    instance.game.getPlayerControll().setPlayer(localPlayer);
                }
                
                // Razmena pozicija izmedju igraca. Salje poziciju
                instance.sendPlayerPositionThread = new Thread(()->{
                    while (GameClient.STATUS) {                        
                        if(instance.playerNumber == 1) {
                            try {
                                instance.toServer.writeUTF("Player:" + instance.game.getPlayerDown().getX());
                                if(!instance.isStartedSend && instance.game.isGameStarted()) {
                                    instance.toServer.writeUTF("Game:START");
                                    instance.isStartedSend = true;
                                }
                                if(instance.isStartedSend && !instance.game.isGameStarted()) {
                                    instance.toServer.writeUTF("Game:STOP");
                                    instance.isStartedSend = false;
                                }
                            } catch (IOException ex) {
                                ex.printStackTrace();
                                
                            }
                        } else {
                            try {
                                instance.toServer.writeUTF("Player:" + instance.game.getPlayerUp().getX());
                            } catch (IOException ex) {
                                ex.printStackTrace();
                                
                            }
                        }
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(GameClient.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                
                instance.sendPlayerPositionThread.start();
                
                // Razmena pozicija izmedju igraca. Prima poziciju i postavlja suprotnog igraca
                instance.reciveOthersPlayerPositionThread = new Thread(()->{
                    while (GameClient.STATUS) {                        
                        if(instance.playerNumber == 1) {
                            try {
                                String input = instance.fromServer.readUTF();
                                String[] inputArray = input.split(":");
                                if(inputArray[0].equals("Player")) {
                                    double x = Double.valueOf(inputArray[1]);
                                    instance.game.getPlayerUp().setX(x);
                                }
                            } catch (IOException ex) {
                                ex.printStackTrace();
                                
                            }
                        } else {
                            try {
                                String input = instance.fromServer.readUTF();
                                String[] inputArray = input.split(":");
                                if(inputArray[0].equals("Player")) {
                                    double x = Double.valueOf(inputArray[1]);
                                    instance.game.getPlayerDown().setX(x);
                                }
                                if(inputArray[0].equals("Game")) {
                                    if(inputArray[1].equals("START"))
                                        instance.game.setGameStarted(true);
                                    if(inputArray[1].equals("STOP"))
                                        instance.game.setGameStarted(false);
                                }
                                if(inputArray[0].equals("Ball")) {
                                    double ballX = Double.valueOf(inputArray[1]);
                                    double ballY = Double.valueOf(inputArray[2]);
                                    double dX = Double.valueOf(inputArray[3]);
                                    double dY = Double.valueOf(inputArray[4]);
                                    double spin = Double.valueOf(inputArray[5]);
                                    instance.game.getPane().setDX(dX);
                                    instance.game.getPane().setDY(dY);
                                    instance.game.getPane().getBall().setX(ballX);
                                    instance.game.getPane().getBall().setX(ballY);
                                    instance.game.getPane().setSpin(spin);
                                }
                            } catch (IOException ex) {
                                ex.printStackTrace();
                                
                            }
                        }
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(GameClient.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                
                instance.reciveOthersPlayerPositionThread.start();
                
                // Razmena pozicija izmedju igraca. Prima poziciju i postavlja suprotnog igraca
//                instance.sendBallPositionThread = new Thread(()->{
//                    while (GameClient.STATUS) {                        
//                        if(instance.playerNumber == 1) {
//                            try {
//                                if(instance.game.isGameStarted()) {
//                                    double ballX = instance.game.getPane().getBall().getX();
//                                    double ballY = instance.game.getPane().getBall().getY();
//                                    double dX = instance.game.getPane().getDX();
//                                    double dY = instance.game.getPane().getDY();
//                                    double spin = instance.game.getPane().getNewSpin();
//                                    instance.toServer.writeUTF("Ball:" + ballX + ":" + ballY + ":" + dX + ":" + dY + ":" + spin);
//                                }
//                            } catch (IOException ex) {
//                                ex.printStackTrace();
//                                
//                            }
//                        }
//                        try {
//                            Thread.sleep(5);
//                        } catch (InterruptedException ex) {
//                            Logger.getLogger(GameClient.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                });
//                
//                instance.sendBallPositionThread.start();
                
            } catch (IOException ex) {
                ex.printStackTrace();
                                
            }
        });
        
        instance.initThread.start();
        
    }
    
    /**
     * Pokusava da zaustavi sve niti
     * 
     * @return void
     */
    public static void stop() {
        create();
        
        // Prekida while cikluse za komunikaciju igraca
        GameClient.STATUS = false;
        
        // Pokusava da ugasi sve niti
        if(instance.initThread != null
                && instance.sendPlayerPositionThread != null
                && instance.reciveOthersPlayerPositionThread != null) {
            
            if(instance.initThread.getState() != Thread.State.TERMINATED)
                instance.initThread.interrupt();

            if(instance.sendPlayerPositionThread.getState() != Thread.State.TERMINATED)
                instance.sendPlayerPositionThread.interrupt();

            if(instance.reciveOthersPlayerPositionThread.getState() != Thread.State.TERMINATED)
                instance.reciveOthersPlayerPositionThread.interrupt();
            
            if(instance.sendBallPositionThread.getState() != Thread.State.TERMINATED)
                instance.sendBallPositionThread.interrupt();
        }
    }
    
    /**
     * Vraca status clijenta
     * 
     * @return boolean
     */
    public static boolean getStatus() {
        return STATUS;
    }
    
    /**
     * Vraca boj igraca 1/2
     * 
     * @return Broj igrace 1/2 up/down
     */
    public static int getPlayerNumber() {
        return instance.playerNumber;
    }
}
