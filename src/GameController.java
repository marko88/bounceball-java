import javafx.scene.Scene;

/**
 * Klasa za pokretanje igre, definise svaki tip igre i postavlja trenutnu igru
 * putem metode setGame()
 *
 * @author Marko
 */
class GameController {
    private static GameController instance = null;

    private Game currentGame;
    private final Game singlePlayerGame;
    private final Game multiPlayerGame;
    
    private final ServerPane serverPane;
    
    private final Scene singlePlayerScene;
    private final Scene multiPlayerScene;
    private final Scene serverScene;
    
    public static void create() {
        if(instance == null)
            instance = new GameController();
    }
    
    private GameController() {
        multiPlayerGame = new MultiPlayerGame();
        multiPlayerScene = new Scene(multiPlayerGame.getPane(), GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        singlePlayerGame  = new SinglePlayerGame();
        singlePlayerScene = new Scene(singlePlayerGame.getPane(), GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        
        serverPane = new ServerPane();
        serverScene = new Scene(serverPane, GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
    }
    
    /**
     * Postavlja i pokrece trazenu igru
     * 
     * @param gameType Tip igre za pokretanje
     */
    public static void setGame(GameTypes gameType) {
        stopGame();
        
        if(gameType == GameTypes.SinglePlayerGame) {
            instance.currentGame = instance.singlePlayerGame;
            getPane().setAllStrtingPositions();
            instance.currentGame.getPlayerControll().setControllsOn();
        }
        
        if(gameType == GameTypes.ServerGame) {
            instance.currentGame = instance.multiPlayerGame;
            GameServer.start(GameMenu.getCreateServerMenuPane().getPort());
//            String nick = GameMenu.getCreateServerMenuPane().getTfNick().getText();
//            int port = GameMenu.getCreateServerMenuPane().getPort();
//            GameClient.start(instance.multiPlayerGame, nick, "localhost", port);
//            getPane().setAllStrtingPositions();
//            instance.currentGame.getPlayerControll().setControllsOn();
        }
        
        if(gameType == GameTypes.ClientGame) {
            instance.currentGame = instance.multiPlayerGame;
            String nick = GameMenu.getJoinMenuPane().getTfNick().getText();
            String host = GameMenu.getJoinMenuPane().getTfHost().getText();
            int port = GameMenu.getJoinMenuPane().getPort();
            GameClient.start(instance.multiPlayerGame, nick, host, port);
            getPane().setAllStrtingPositions();
            instance.currentGame.getPlayerControll().setControllsOn();
        }
        
        // Give controll to current game
        if(instance.currentGame != null)
            instance.currentGame.getPlayerControll().setControllsOn();
    }
    
    /**
     * Zaustavlja trenutnu igru
     * 
     * @return void
     */
    public static void stopGame() {
        if(instance.currentGame != null) {
            instance.currentGame.stop();
            instance.currentGame.getPlayerControll().setControllsOff();
            instance.currentGame.setGameStarted(false);
        }
    }

    /**
     * Dali je pocela igra
     * 
     * @return boolean
     */
    public static boolean isGameStarted() {
        return instance.currentGame.isGameStarted();
    }

    /**
     * Postavlja stanje igre
     * 
     * @param gameStarted 
     */
    public static void setGameStarted(boolean gameStarted) {
        instance.currentGame.setGameStarted(gameStarted);
    }
    
    /**
     * Vraca platno trenutne igre
     * 
     * @return BallPane
     */
    public static BallPane getPane() {
        return instance.currentGame.getPane();
    }
    
    /**
     * Vraca platno SinglePlayer igre
     * 
     * @return BallPane
     */
    public static BallPane getSinglePlayerPane() {
        return instance.singlePlayerGame.getPane();
    }

    /**
     * Vraca SinglePlayer Scene objekat
     * 
     * @return Scene
     */
    public static Scene getSinglePlayerScene() {
        getPane().setAllStrtingPositions();
//        instance.currentGame.getControll().setControllsOn();
        return instance.singlePlayerScene;
    }
    
    /**
     * Vraca platno MultiPlayer igre
     * 
     * @return BallPane
     */
    public static BallPane getMultiPlayerPane() {
        return instance.multiPlayerGame.getPane();
    }

    /**
     * Vraca MultiPlayer Scene objekat
     * 
     * @return Scene
     */
    public static Scene getMultiPlayerScene() {
        getPane().setAllStrtingPositions();
//        instance.currentGame.getControll().setControllsOn();
        return instance.multiPlayerScene;
    }

    /**
     * Vraca scene objekat servera
     * 
     * @return Scene
     */
    public static Scene getServerScene() {
        return instance.serverScene;
    }

    /**
     * Vraca platno servera
     * 
     * @return ServerPane
     */
    public static ServerPane getServerPane() {
        return instance.serverPane;
    }
    
}

enum GameTypes {
    SinglePlayerGame,
    ServerGame,
    ClientGame    
};