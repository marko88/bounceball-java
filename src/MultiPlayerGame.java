/**
 * Klasa za podesavanje multiplayer igre.
 *
 * @author marko
 */
public class MultiPlayerGame extends Game {
    
    @Override
    protected void setup() {
            setPlayerUp( new Player("Player1", Player.UP) );
            setPlayerDown( new Player("Player2", Player.DOWN) );
            setPane( new BallPane(getPlayerUp(), getPlayerDown()) );
            setPlayerControll( new PlayerControll(getPane(), getPlayerDown()) );
    }
    
}
