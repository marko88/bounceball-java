
import javafx.scene.Scene;
import javafx.scene.paint.Color;

/**
 * Klasa za generisanje svih menija i snimanje Scene objekata za svaki meni
 * pojedinacno.
 * Sluzi za dobijanje platna i scene za sve dostupne menije.
 * 
 * Singleton klasa.
 *
 * @author marko
 */
class GameMenu {
    
    private static GameMenu instance;
    
    private final Scene mainMenuScene;
    private final Scene multiPlayerMenuScene;
    private final Scene createServerMenuScene;
    private final Scene joinMenuScene;
    
    private MainMenuPane mainMenuPane;
    private MultiPlayerMenuPane multiPlayerMenuPane;
    private CreateJoinMenuPane createServerMenuPane;
    private CreateJoinMenuPane joinMenuPane;

    static void create() {
        instance = new GameMenu();
    }
    
    private GameMenu() {
        mainMenuPane = new MainMenuPane();
        multiPlayerMenuPane = new MultiPlayerMenuPane();
        mainMenuScene = new Scene(mainMenuPane, GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        multiPlayerMenuScene = new Scene(multiPlayerMenuPane, GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        createServerMenuPane = new CreateJoinMenuPane(Server.CREATE);
        createServerMenuScene = new Scene(createServerMenuPane, GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        joinMenuPane = new CreateJoinMenuPane(Server.JOIN);
        joinMenuScene = new Scene(joinMenuPane, GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        
        // Postavlja promenu boja za elemente menija
        setOnMouseOverForMainMenu();
        setOnMouseOverForMultiplayerMenu();
        setOnMouseOverForCreateServerMenu();
        setOnMouseOverForJoinMenu();
    }
    
    /**
     * Podesava OnMouseOver promenu boja za glavni meni
     * 
     * @return void
     */
    private void setOnMouseOverForMainMenu() {
        mainMenuPane.getSinglePlayerLabel().setOnMouseMoved(e -> {
            mainMenuPane.getSinglePlayerLabel().setTextFill(Color.WHITE);
            mainMenuPane.getMultiPlayerLabel().setTextFill(Color.DARKGREY);
            mainMenuPane.getExitLabel().setTextFill(Color.DARKGREY);
        });
        mainMenuPane.getMultiPlayerLabel().setOnMouseMoved(e -> {
            mainMenuPane.getSinglePlayerLabel().setTextFill(Color.DARKGREY);
            mainMenuPane.getMultiPlayerLabel().setTextFill(Color.WHITE);
            mainMenuPane.getExitLabel().setTextFill(Color.DARKGREY);
        });
        mainMenuPane.getExitLabel().setOnMouseMoved(e -> {
            mainMenuPane.getSinglePlayerLabel().setTextFill(Color.DARKGREY);
            mainMenuPane.getMultiPlayerLabel().setTextFill(Color.DARKGREY);
            mainMenuPane.getExitLabel().setTextFill(Color.WHITE);
        });
    }
    
    /**
     * Podesava OnMouseOver promenu boja za multiplayer meni
     * 
     * @return void
     */
    private void setOnMouseOverForMultiplayerMenu() {
        multiPlayerMenuPane.getCreateServerLabe().setOnMouseMoved(e -> {
            multiPlayerMenuPane.getCreateServerLabe().setTextFill(Color.WHITE);
            multiPlayerMenuPane.getJoinLabel().setTextFill(Color.DARKGREY);
            multiPlayerMenuPane.getBackLabel().setTextFill(Color.DARKGREY);
        });
        multiPlayerMenuPane.getJoinLabel().setOnMouseMoved(e -> {
            multiPlayerMenuPane.getCreateServerLabe().setTextFill(Color.DARKGREY);
            multiPlayerMenuPane.getJoinLabel().setTextFill(Color.WHITE);
            multiPlayerMenuPane.getBackLabel().setTextFill(Color.DARKGREY);
        });
        multiPlayerMenuPane.getBackLabel().setOnMouseMoved(e -> {
            multiPlayerMenuPane.getCreateServerLabe().setTextFill(Color.DARKGREY);
            multiPlayerMenuPane.getJoinLabel().setTextFill(Color.DARKGREY);
            multiPlayerMenuPane.getBackLabel().setTextFill(Color.WHITE);
        });
    }
    
    /**
     * Podesava OnMouseOver promenu boja za Create Server meni
     * 
     * @return void
     */
    private void setOnMouseOverForCreateServerMenu() {
        createServerMenuPane.getCreateJoinLabel().setOnMouseMoved(e -> {
            createServerMenuPane.getCreateJoinLabel().setTextFill(Color.WHITE);
            createServerMenuPane.getBackLabel().setTextFill(Color.DARKGREY);
        });
        createServerMenuPane.getBackLabel().setOnMouseMoved(e -> {
            createServerMenuPane.getCreateJoinLabel().setTextFill(Color.DARKGREY);
            createServerMenuPane.getBackLabel().setTextFill(Color.WHITE);
        });
        createServerMenuPane.getConfigPane().setOnMouseMoved(e -> {
            createServerMenuPane.getCreateJoinLabel().setTextFill(Color.DARKGREY);
            createServerMenuPane.getBackLabel().setTextFill(Color.DARKGREY);
        });
    }
    
    /**
     * Podesava OnMouseOver promenu boja za Join meni
     * 
     * @return void
     */
    private void setOnMouseOverForJoinMenu() {
        joinMenuPane.getCreateJoinLabel().setOnMouseMoved(e -> {
            joinMenuPane.getCreateJoinLabel().setTextFill(Color.WHITE);
            joinMenuPane.getBackLabel().setTextFill(Color.DARKGREY);
        });
        joinMenuPane.getBackLabel().setOnMouseMoved(e -> {
            joinMenuPane.getCreateJoinLabel().setTextFill(Color.DARKGREY);
            joinMenuPane.getBackLabel().setTextFill(Color.WHITE);
        });
        joinMenuPane.getConfigPane().setOnMouseMoved(e -> {
            joinMenuPane.getCreateJoinLabel().setTextFill(Color.DARKGREY);
            joinMenuPane.getBackLabel().setTextFill(Color.DARKGREY);
        });
    }

    /**
     * Vraca platno glavnog menija
     * 
     * @return MainMenuPane
     */
    public static MainMenuPane getMainMenuPane() {
        return instance.mainMenuPane;
    }

    /**
     * Vraca scenu glavnog menija
     * 
     * @return Scene
     */
    public static Scene getMainMenuScene() {
        return instance.mainMenuScene;
    }

    /**
     * Vraca multiplayer meni scene objekat
     * 
     * @return Scene
     */
    public static Scene getMultiPlayerMenuScene() {
        return instance.multiPlayerMenuScene;
    }

    /**
     * Vraca platno multiplayer menija
     * 
     * @return MultiPlayerMenuPane
     */
    public static MultiPlayerMenuPane getMultiPlayerMenuPane() {
        return instance.multiPlayerMenuPane;
    }

    /**
     * Vraca Create Server meni Scene objekat
     * 
     * @return Scene
     */
    public static Scene getCreateServerMenuScene() {
        return instance.createServerMenuScene;
    }

    /**
     * Vraca Create server meni platno
     * 
     * @return CreateJoinMenuPane
     */
    public static CreateJoinMenuPane getCreateServerMenuPane() {
        return instance.createServerMenuPane;
    }

    /**
     * Vraca join meni scenu
     * 
     * @return Scene
     */
    public static Scene getJoinMenuScene() {
        return instance.joinMenuScene;
    }

    /**
     * Vraca platno za join meni
     * 
     * @return CreateJoinMenuPane
     */
    public static CreateJoinMenuPane getJoinMenuPane() {
        return instance.joinMenuPane;
    }
    
}
