/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class GameRegistryTest {
    
    public GameRegistryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of set method, of class GameRegistry.
     */
    @Test
    public void testSet() {
        System.out.println("set testing");
        String key = "kljuc";
        String value = "vrednost";
        GameRegistry.create();
        GameRegistry.set(key, value);
        assertEquals(GameRegistry.get(key), value);
    }

    /**
     * Test of get method, of class GameRegistry.
     */
    @Test
    public void testGet() {
        System.out.println("get testing");
        String key = "kljuc";
        String expResult = "vrednost";
        GameRegistry.create();
        GameRegistry.set(key, "vrednost");
        String result = GameRegistry.get(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of getInt method, of class GameRegistry.
     */
    @Test
    public void testGetInt() {
        System.out.println("getInt testing");
        String key = "kljuc";
        int expResult = 12;
        GameRegistry.create();
        GameRegistry.set(key, "12");
        int result = GameRegistry.getInt(key);
        assertEquals(expResult, result);
    }
    
}
