
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Glavni/Osonvni meni igre.
 * Pravi platno menija.
 * 
 * @author marko
 */
class MainMenuPane extends VBox {
    private final Label singlePlayerLabel;
    private final Label multiPlayerLabel;
    private final Label exitLabel;

    public MainMenuPane() {
        setWidth(GameRegistry.getInt("WINDOW_WIDTH"));
        setHeight(GameRegistry.getInt("WINDOW_HEIGHT"));
        setPrefSize(GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        setStyle("-fx-background-color: #000;");
        
        singlePlayerLabel = new Label("Singleplayer");
        multiPlayerLabel = new Label("Multiplayer");
        exitLabel = new Label("Exit");
        
        singlePlayerLabel.setPadding(new Insets(10));
        multiPlayerLabel.setPadding(new Insets(10));
        exitLabel.setPadding(new Insets(10));
        
        singlePlayerLabel.setTextFill(Color.DARKGREY);
        multiPlayerLabel.setTextFill(Color.DARKGREY);
        exitLabel.setTextFill(Color.DARKGREY);
        
        singlePlayerLabel.setFont(new Font(20));
        multiPlayerLabel.setFont(new Font(20));
        exitLabel.setFont(new Font(20));
        
        setAlignment(Pos.CENTER);
        
        getChildren().addAll(singlePlayerLabel, multiPlayerLabel, exitLabel); // Postavljanje menija
    }

    /**
     * Vraca labelu za sigleplayer.
     * 
     * @return Label
     */
    public Label getSinglePlayerLabel() {
        return singlePlayerLabel;
    }

    /**
     * Vraca labelu za multiplayer.
     * 
     * @return Label
     */
    public Label getMultiPlayerLabel() {
        return multiPlayerLabel;
    }

    /**
     * Vraca labelu za exit.
     * 
     * @return Label
     */
    public Label getExitLabel() {
        return exitLabel;
    }
    
}
