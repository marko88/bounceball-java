
import java.util.Map;
import java.util.TreeMap;

/**
 * Klasa za snimanje opstih vrednosti aplikacije.
 * 
 * Singleto klasa.
 *
 * @author marko
 */
class GameRegistry {
    private static GameRegistry instance;
    private Map<String, String> map;
    
    private GameRegistry() {
        map = new TreeMap<>();
    }

    /**
     * Pravi unutrasnju instancu.
     * 
     * @return void
     */
    public static void create() {
        if(instance == null)
            instance = new GameRegistry();
    }
    
    /**
     * Ubacuje string kljuc/vrednost u Registar.
     * 
     * @param key Kljuc
     * @param value Vrednost
     */
    public static void set(String key, String value) {
        instance.map.put(key, value);
    }
    
    /**
     * Uzima vrednost trazenog kljuca.
     * 
     * @param key Kljuc
     * @return String
     */
    public static String get(String key) {
        return instance.map.get(key);
    }
    
    /**
     * Uzima vrednost trazenog kljuca u vidu integer-a.
     * 
     * @param key Kljuc
     * @return int
     */
    public static int getInt(String key) {
        return Integer.valueOf(get(key));
    }
    
}
