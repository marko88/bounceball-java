
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Meni za Create Server i Join.
 * Pravi platno menija.
 *
 * @author marko
 */
class CreateJoinMenuPane extends VBox {
    private final Label nickLabel;
    private final Label hostLabel;
    private final Label portLabel;
    private final Label createJoinLabel;
    private final Label backLabel;
    private final TextField tfNick;
    private final TextField tfHost;
    private final TextField tfPort;
    private final GridPane configPane;
    private final boolean isServer;

    public CreateJoinMenuPane(Server type) {
        // Postavlja velicinu platna
        setWidth(GameRegistry.getInt("WINDOW_WIDTH"));
        setHeight(GameRegistry.getInt("WINDOW_HEIGHT"));
        setPrefSize(GameRegistry.getInt("WINDOW_WIDTH"), GameRegistry.getInt("WINDOW_HEIGHT"));
        setStyle("-fx-background-color: #000;");
        
        isServer = (type == Server.CREATE) ? true : false;
        
        // Podesavanje dugmica i labela
        nickLabel = new Label("Nick:");
        hostLabel = new Label("Host:");
        portLabel = new Label("Port:");
        createJoinLabel = new Label("Start");
        if(type == Server.JOIN)
            createJoinLabel.setText("Join");
        backLabel = new Label("Back");
        tfNick = new TextField();
        if(type == Server.JOIN)
            tfNick.setText("Player");
        tfHost = new TextField("localhost");
        tfPort = new TextField("7325");
        
        // Podesava platno sa opcijama
        configPane = new GridPane();
        if(type == Server.JOIN)
            configPane.setMaxWidth(530);
        else
            configPane.setMaxWidth(520);
        if(type == Server.JOIN) {
            configPane.add(nickLabel, 0, 0);
            configPane.add(tfNick, 1, 0);
            configPane.add(hostLabel, 0, 1);
            configPane.add(tfHost, 1, 1);
        }
        configPane.add(portLabel, 0, 2);
        configPane.add(tfPort, 1, 2);
        configPane.setPadding(new Insets(0, 0, 20, 150));
        configPane.setHgap(5);
        configPane.setVgap(15);
        
        // Podesavanje labela(font, boja i margine)+
        
        createJoinLabel.setPadding(new Insets(10));
        backLabel.setPadding(new Insets(10));
        nickLabel.setTextFill(Color.LIGHTGRAY);
        hostLabel.setTextFill(Color.LIGHTGRAY);
        portLabel.setTextFill(Color.LIGHTGRAY);
        createJoinLabel.setTextFill(Color.DARKGREY);
        backLabel.setTextFill(Color.DARKGREY);
        nickLabel.setFont(new Font(16));
        hostLabel.setFont(new Font(16));
        portLabel.setFont(new Font(16));
        createJoinLabel.setFont(new Font(20));
        backLabel.setFont(new Font(20));
        
        setAlignment(Pos.CENTER);
        
        getChildren().addAll(configPane, createJoinLabel, backLabel); // Postavljanje menija
    }

    /**
     * Vraca labelu porta
     * 
     * @return Label
     */
    public Label getPortLabel() {
        return portLabel;
    }

    /**
     * Vraca labelu back
     * 
     * @return Label
     */
    public Label getBackLabel() {
        return backLabel;
    }

    /**
     * Vraca labelu nick
     * 
     * @return Label
     */
    public Label getNickLabel() {
        return nickLabel;
    }

    /**
     * Vraca labelu hosta
     * 
     * @return Label
     */
    public Label getHostLabel() {
        return hostLabel;
    }

    /**
     * Vraca labelu create/join
     * 
     * @return Label
     */
    public Label getCreateJoinLabel() {
        return createJoinLabel;
    }

    /**
     * Vraca textfield nick
     * 
     * @return TextField
     */
    public TextField getTfNick() {
        return tfNick;
    }

    /**
     * Vraca textfield hosts
     * 
     * @return TextField
     */
    public TextField getTfHost() {
        return tfHost;
    }

    /**
     * Vraca textfield port
     * 
     * @return TextField
     */
    public TextField getTfPort() {
        return tfPort;
    }

    /**
     * Vraca config pane
     * 
     * @return GridPane
     */
    public GridPane getConfigPane() {
        return configPane;
    }

    /**
     * Proverava dali je server, u suprotnom je klijent
     * 
     * @return boolean
     */
    public boolean isServer() {
        return isServer;
    }
    
    /**
     * Vraca port
     * 
     * @return Broj porta
     */
    public int getPort() {
        return (int)Integer.parseInt(tfPort.getText());
    }
    
}

enum Server {
    CREATE,
    JOIN
}
